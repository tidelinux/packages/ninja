# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

DOWNLOAD="https://github.com/ninja-build/ninja/archive/v${BPM_PKG_VERSION}/ninja-${BPM_PKG_VERSION}.tar.gz"
FILENAME="${DOWNLOAD##*/}"

# The prepare function is executed in the root of the temp directory
# This function is used for downloading files and putting them into the correct location
prepare() {
  wget "$DOWNLOAD"
  tar -xvf "$FILENAME" --strip-components=1 -C "$BPM_SOURCE"
}

# The build function is executed in the source directory
# This function is used to compile the source code
build() {
  sed -i '/int Guess/a \
    int   j = 0;\
    char* jobs = getenv( "NINJAJOBS" );\
    if ( jobs != NULL ) j = atoi( jobs );\
    if ( j > 0 ) return j;\
  ' src/ninja.cc
  python3 configure.py --bootstrap
}

# The check function is executed in the source directory
# This function is used to run tests to verify the package has been compiled correctly
check() {
  ./ninja ninja_test
  ./ninja_test --gtest_filter=-SubprocessTest.SetWithLots
}

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  install -vDm755 ninja "$BPM_OUTPUT"/usr/bin/ninja
  install -vDm644 misc/bash-completion "$BPM_OUTPUT"/usr/share/bash-completion/completions/ninja
  install -vDm644 misc/zsh-completion  "$BPM_OUTPUT"/usr/share/zsh/site-functions/_ninja
}
